# Slices

## Arays

The type [n]T is an array of n values of type T.

The expression

```go
var a [10]int
```
declares a variable a as an array of ten integers.

An array's length is part of its type, so arrays cannot be resized. This seems limiting, but don't worry arrays are rarely used on their own. They are an underlying basic data structure on which the Slice is built.

## What are Slices

An array has a fixed size. A slice, on the other hand, is a dynamically-sized, flexible view into the elements of an array. In practice, slices are much more common than arrays. 

The type []T is a slice with elements of type T. 

Slices do not contain data, rather they point at a location of an array in memory.
A slice is formed by specifying two indices, a low and high bound, separated by a colon: 

```go
func main() {
	primes := [6]int{2, 3, 5, 7, 11, 13} // this is an array

	var s []int = primes[1:4] // this is a slice
	fmt.Println(s)
}
```

Changing the elements of a slice modifies the underlying data in the referenced array. Other slices to the same array will be able to see the changes.

```go
func main() {
	nums := [5]int{1, 2, 3, 4, 5}  // this is an array
	fmt.Println(nums)

	a := nums[0:2] // this is a slice
    b := nums[1:4] // this is a slice
    fmt.Println(a, b)

	b[0] = 10
	fmt.Println(a, b)
	fmt.Println(nums)
}
```
### Literal

We can define a slice literal like we would an array but by omitting the size.

```go
a := []int{2, 3, 5, 7, 11, 13}  // this is a slice
fmt.Println(a)

a[0:10]
a[:10]
a[0:]
a[:]   // These slice expressions are equivalent
```

## Length and Capacity

Besides a pointer to the underlying array, a slice also contains *length* and *capacity*.

Length is the number of elements in the slice. 


Capacity is the number of elements in the underlying array, counting from the first element in the slice. 

```go
// we can use 
// len(s) to get the length of a slice
// cap(s) to get the capacity of a slice

func main() {
	s := []int{2, 3, 5, 7, 11, 13}
	printSlice(s)

	// Slice the slice to give it zero length.
	s = s[:0]
	printSlice(s)

	// Extend its length.
	s = s[:4]
	printSlice(s)

	// Drop its first two values.
	s = s[2:]
	printSlice(s)
}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}
```

⚠️ Let's try slicing beyond capacity

### Nil slice

An default, or zero value, slice has the value of nil. Similar to a pointer. It has length and capacity of 0.

## Make

Empty slices can be created using the *make* keyword. 

```go
// make creates and underlying array according to the parameters passed
a := make([]int, 5)     // len(a)=5
b := make([]int, 0, 5)  // len(b)=0, cap(b)=5

// All the elements in the slice are zeroed, or default.
```
<br>

### Slices of slices

We can have multi depth slices containing other slices.

```go
matrix := [][]string{
    []string{"1", "2", "1"},
    []string{"2", "2", "2"},
    []string{"1", "2", "1"},
}
```

Do keep in mind when using create on the root slice the other slices are zeroed, so we must make sure to create them too.


⚒️ Define a slice literal *a*, then create another slice *b* using make and copy all the elements from one to the other in reverse order.

## Appending 

In order to correctly and safely append new elements to a slice we use the *append* keyword. Append takes care of adding the element/s at the end of the slice making sure to resize the underlying array if the capacity would be exceeded. 

```go
func main() {
	var s []int
	printSlice(s)

	// append works on nil slices.
	s = append(s, 0)
	printSlice(s)

	// The slice grows as needed.
	s = append(s, 1)
	printSlice(s)

	// We can add more than one element at a time.
	s = append(s, 2, 3, 4)
	printSlice(s)
}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

```

⚠️ Keep in mind, if the underlying array does get resized the pointer changes

⚒️ (Optional) Implement append

## Range

When iterating over slices and maps we can use the *range* keyword in for.

```go
func main(){
    s := []int{2, 3, 5, 7, 11, 13}
    for i, v := range s {              // First return value is the index and the second is the value
		fmt.Printf("index=%d value=%d\n", i, v)
	}

    for i := range pow        // We can ommit the value 
    for _, value := range pow  // We can ignore the index
}
```

## Can we pass slices as arguments?

As we found out slices are actually a data structure that keep a reference to an array along with it's length and capacity, so passing a slice argument copies the structure with a pointer in it so changes to the elements show up in the caller.

```go
func mutator(strs []string) {
    strs[0]="z"
}

func main(){
    strs := []string{"a","b","c"}
    mutator(strs)
    fmt.Println(strings.Join(strs, ","))
}
```
What if we add some elements.

🤔 What's the issue with this code

```go
func mutator(strs []string) {
    strs[0]="z"
    strs = append(strs, "sike")
}

func main(){
    strs := []string{"a","b","c"}
    mutator(strs)
    fmt.Println(strings.Join(strs, ","))
}
```
⚒️ Make the code work correctly


## Variadic functions

Variadic functions can take any number of trailing arguments of the same type.

```go
func add(numbers ...int) int {
    total := 0
    for _, num := range numbers{
        total += num
    }

    return total
}
```
You split a slice when calling a variadic function like so.
```go
func printer(stuff ...string) {
	fmt.Println(stuff)
}

func main() {
	list := []string {"abc", "bca"}
	printer(list...)
}
```

A very common variadic function we already used is ***fmt.Println*** which accepts any number of string to print. 

## Exercises

⚒️ Let's create a function that takes a variable number of numbers and adds 2 to every element and returns the slice. Let's call it ***improver***

⚒️ Modify ***improver*** so it takes the number it has to add as an argument.

⚒️ Let's add a new function that checks if the numbers are in order, our ***improver*** will use it to check if the numbers are in order and if not will return an error.

⚒️ Let's modify main so it prints special messages if it receives and error from function calls

⚒️ Let's create a function that prints the slice

# Next
[Maps ➡️](maps.md)
