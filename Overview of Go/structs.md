# Structs

- [Structs](#structs)
	- [Defining](#defining)
	- [Pointers](#pointers)
	- [Returning pointers](#returning-pointers)
- [Next](#next)

## Defining

A struct is a data type defined by us the developer, that contains a collection of fields. These fields can have basic data types or other structs.

```go
type User struct {
	name   string
	active bool
}

func main() {
	user := User{
		name:   "John",
		active: false,
	}
	user.active = true
    fmt.Println(user)
}
```

Structs are **not** classes, they do not have implicit constructors and they sometimes don't behave like an object would in an OOP language. If a struct is created without providing values for it's fields, the fields are initialized with default values. 

```go
user := User{} // name = "" active = false
var user User  // name = "" active = false

// The two commands are equivalent
```

This is because a struct is **only** a bucket of structured data, for now, so if not initialized it's an empty bucket of data.

## Pointers

We can have pointers to structs like we can any value, this is useful when passing complex data around. The language allows us to access pointer struct fields without having to add  **\*** to make it less cumbersome.

```go
type User struct {
	name   string
	active bool
}

func main() {
	user := User{
		name:   "John",
		active: false,
	}
	p := &user
    p.active = true  // instead of (*p).active
    fmt.Println(user)
}
```

## Returning pointers

When working with complex structures it makes sense sometime to pass around pointers to avoid copying the data over and over. A common example is having a creator function which returns a pointer to a newly created struct.

```go
type User struct {
	name   string
	active bool
}

func createUser(name string) *User {
    return &User{
        name: string,
        active: false,
    }
}

func main() {
	user := createUser("John")
    user.active = true
    fmt.Println(user)
}
```

<details><summary>⚠️ Some of you might already be alerted to something that would've caused a bug in C.</summary>

In C a similar code woud'v created a dangling pointer. A variable which points to data that was freed when it exited the scope of the creating function.

This fortunatly doesn't happen in Go. The compiler automatically picks up on this behavior and the garbage collector does not free the memory.

</details>

⚒️ Create a structure called House which contains a string address and a pointer to a User owner.
```
House{
    owner - pointer to a User
    address - string
}
```

⚒️ Create a function which has a User pointer argument and creates and returns a House pointer which has the user set as owner.

```go
func createHouse(user *User) *House{
    ...
}
```

⚒️ Change the active status in the created User and check if the User from the House pointer also has it's status 
changed

```go
user := createUser("John")
house := createHouse(user)
user.active = true
house.owner.active = ???
```

# Next
[Slices ➡️](slices.md)