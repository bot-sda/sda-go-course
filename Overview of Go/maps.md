# Maps

The map structure contains keys and values with a 1-1 relation between them.
A zero value, or default, of a map is nil. A nil map has no keys and keys cannot be added to it.

## Make

In order to create an empty map we also use the make keyword.

```go
m := make(map[int]string)

m := make(map[string]string)

m := make(map[*Variant]*Publications)
```

## Literal

```go
m := map[string]int{
	"turnip": 10,
	"Google": 9,
}
fmt.Println(m)
```

## Operations

```go
// Insert or update element
m[key] = elem

// Retrieve element
elem := m[key]

// Delete element
delete(m, key)

// Test that key exists
elem, ok := m[key]

// Test that key exists and print it
if elem, ok := m[key]; ok {
    fmt.Println(elem)
}

```

⚒️ Implement a function that returns a map with the count of each word in a string.

<details><summary>Example Text</summary>

```
You have to learn to pace yourself
Pressure
You're just like everybody else
Pressure
You've only had to run so far
So good
But you will come to a place
Where the only thing you feel
Are loaded guns in your face
And you'll have to deal with
Pressure
You used to call me paranoid
Pressure
But even you can not avoid
Pressure
You turned the tap dance into your crusade
Now here you are with your faith
And your Peter Pan advice
You have no scars on your face
And you cannot handle pressure
All grown up and no place to go
Psych 1, Psych 2
What do you know?
All your life is Channel 13
Sesame Street
What does it mean?
I'll tell you what it means
Pressure
Pressure
Don't ask for help
You're all alone
Pressure
You'll have to answer
To your own
Pressure
I'm sure you'll have some cosmic rationale
But here you are in the ninth
Two men out and three men on
Nowhere to look but inside
Where we all respond to
Pressure
Pressure
All your life is Time magazine
I read it too
What does it mean?
Pressure
I'm sure you'll have some cosmic rationale
But here you are with your faith
And your Peter Pan advice
You have no scars on your face
And you cannot handle pressure
Pressure, pressure
One, two, three, four
Pressure
```

</details>

# Next
[Packages ➡️](packages.md)
