# Pointers

- [Pointers](#pointers)
  - [What is a pointer](#what-is-a-pointer)
  - [Why use a pointer](#why-use-a-pointer)
  - [Ok now how](#ok-now-how)
  - [Arguments by value or reference](#arguments-by-value-or-reference)
- [Next](#next)

Go is full of pointers 😰 and it's use in everyday code is encouraged. 

That's why a good understanding of pointers is recommended before diving in. The good part is many of the design choices that allowed you to shoot yourself in the foot in C and C++ are *fixed* in Go.

Also we have garbage collection, this makes our lives much easier.

In Go there is no pointer arithmetic.

## What is a pointer

A pointer is a variable that stores the address in memory of a value. This is transparent to us, we just pass around the address of the value without actually seeing the address.

## Why use a pointer

When working with data that needs to be modified from different places of the software we'll need to pass pointers around to make sure all callers refer to the same underlying data. 

In a language like Java or C# pointers are implicit, when working with objects the language in the back passes pointers around, this is great but offers less control over the data and it's lifetime which is sometimes needed in development.

## Ok now how

If we have a variable we can extract it's pointer using the operator **&**.
If we want to access the data from a pointers location we use the operator **\***
```go
i, j := 30, 3909

p := &i         // p now has a pointer to i
fmt.Println(*p) // read i through the pointer
*p = 21         // set i through the pointer
fmt.Println(i)  // see the new value of i

p = &j         // point to j
*p = *p / 37   // divide j through the pointer
fmt.Println(j) // see the new value of j
```

## Arguments by value or reference

Well everything is passed by value in go. That's where pointers come in.

Passing a pointer as an argument to a function allows you to make changes to the underlying data, which the pointer references, and have those changes reflect in the caller.

Why?<br>

Because while Go creates a copy of the pointer(which is only an address) the address it points to is the same for the caller and the function.

```go
func mutator(num *int) {
    *num += 3
}

func main(){
    num := 0
    mutator(&num)
    fmt.Println(num)
}
```

⚒️ Create a function that swaps two numbers in place, without returning anything. TIP You'll probably need pointers.

# Next
[Structs ➡️](structs.md)