# Variables

- [Variables](#variables)
  - [Var](#var)
  - [Initializers](#initializers)
- [Basic Types](#basic-types)
  - [Constants](#constants)
  - [Type conversion](#type-conversion)
  - [Printing](#printing)
- [Next](#next)


## Var
The keyword ***var*** defines a list of variables, and in Go the variable type is last.

```go
var name, surname string

func main(){
    var i int
    fmt.Println(i, name, surname)
}
```

A var statement can be used at package or function level.

## Initializers

Variables can be initialized at declaration, if we provide an initializer we can omit the type.

```go
var me, you, learning = "John", "Doe", true
```

Inside a function the ***:=*** assignment statement can be used in place of var. Do keep in mind this only works for initializations, simple assignments use ***=***.

```go
a := 1
b, c := 2, 3
a = 3
```

⚒️ Initialize two numbers with some values then initialize a third with the sum of the first two.

# Basic Types

Go's basic types

```go
bool

string

int  int8  int16  int32  int64
uint uint8 uint16 uint32 uint64 uintptr

byte // alias for uint8
     // used extensively when working with data streams 

rune // alias for int32
     // represents a Unicode code point

float32 float64

complex64 complex128
```
 Variables declared without an explicit initial value are given their zero value.

The zero value is:
- 0 for numeric types,
- false for the boolean type, and
- "" (the empty string) for strings.

## Constants
Constants are declared like variables, but with the const keyword. 

```go
const Smile = "😁"
const Pi = 3.14
```

## Type conversion

To convert a value *v* to type *T* we use the ***T(v)*** expression

```go
var x, y int = 3, 4
var f float64 = math.Sqrt(float64(x*x + y*y))
var z uint = uint(f)
fmt.Println(x, y, z)
```

Failing to provide a conversion expression throws a compilation error.

## Printing
To convert a number to a string we actually use the fmt package.

```go
num := 10

str := "Party starts at" + num + " come hungry!"            // ERROR
str := fmt.Sprintf("Party starts at %d come hungry!", num)  // CORRECT
```

Other valuable verbs include

```go
%v	the value in a default format
	when printing structs, the plus flag (%+v) adds field names
%#v	a Go-syntax representation of the value
%T	a Go-syntax representation of the type of the value
%%	a literal percent sign; consumes no value

%b	base 2
%d	base 10
%o	base 8

%s	the uninterpreted bytes of the string or slice
%q	a double-quoted string safely escaped with Go syntax
```

You can find all verbs at https://pkg.go.dev/fmt#hdr-Printing

⚒️ Initialize two numbers one as an *int* 20 and one as a *float* 1.5, add them together and then print them in this sentence 

```
"The results came in at 21.5%. Splendid!"
```

# Next
[Flow ➡️](flow.md)