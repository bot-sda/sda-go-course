# Table of Contents
- [Table of Contents](#table-of-contents)
- [Methods](#methods)
    - [Pointer receivers](#pointer-receivers)
- [Interfaces](#interfaces)
  - [The empty interface](#the-empty-interface)
  - [Type assertion](#type-assertion)
    - [Type switch](#type-switch)
  - [Standard Interfaces](#standard-interfaces)
    - [Errors](#errors)
    - [Readers](#readers)
    - [io.Reader wrapper](#ioreader-wrapper)
  - [File IO](#file-io)
- [Next](#next)


# Methods

In Go methods are functions defined with a type receiver argument. Even tough there are no classes in Go we can still have functions that work with structs or any other data type.

The receiver appears as its own argument before the method name.

```go
type IntTuple struct {
    X, Y int
}

func (t IntTuple) Sum() int {
    return t.X + t.Y
}

func main(){
    t := IntTuple{2,3}
    fmt.Println(t.Sum())
}

```
You can only declare a method with a receiver whose type is defined in the same package as the method. You cannot declare a method with a receiver whose type is defined in another package. So for basic data types we want to create a method for, we'll need to redeclare a local type.


```go
type MyFloat float64

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

func main() {
	f := MyFloat(-math.Sqrt2)
	fmt.Println(f.Abs())
}
```

Let's look at the following method

```go
type IntTuple struct {
    X, Y int
}

func (t IntTuple) Swap() {
    aux := t.X
    t.X = t.Y
    t.Y = aux
}

func main(){
    t := IntTuple{2,3}
    t.Swap()
    fmt.Println(t)
}
```

⚠️ Does this behave correctly? If not why.

<details><summary>Hint</summary>
Arguments in Go are passed by value, even receiver arguments.
</details>
<br>

⚒️ Fix the code so that the method works correctly



<details><summary>Solution</summary>

### Pointer receivers

We can declare methods that have a pointer receiver. Methods with pointer receivers can modify the value to which the receiver points.

```go
type IntTuple struct {
    X, Y int
}

func (t *IntTuple) Swap() {
    aux := t.X
    t.X = t.Y
    t.Y = aux
}

func main(){
    t := IntTuple{2,3}
    t.Swap()
    fmt.Println(t)
}

```

In general it is recommended to keep all methods for a type either value or pointer receivers, and not a mixture of both.

</details><br>


# Interfaces

An *interface* type is defined as a set of method signatures. A value of interface can hold any value that implements those methods.

Unlike OOP languages in which interface implementation is explicit using keywords like "implements", in go interface implementations are implicit. This means there is no special keyword, if a type satisfies the methods in an interface it automatically implements it.


```java
// Java
public class Pig implements Animal {
...
}
```

```csharp
// C#
class Pig : IAnimal 
{
...
}
```

```go
// Go

type Animal interface {
    MakeSound()
}

type Pig struct{
    Weight uint
    Length uint
}

func (pig Pig) MakeSound(){
    fmt.Println("Oink")
}

func main(){
    var animal Animal = Pig{100,2}
    animal.MakeSound()
}
```

 Implicit interfaces decouple the definition of an interface from its implementation, which could then appear in any package without prearrangement. 

 This also means there is not a real limit to how many interfaces a type can implement, if it has the methods it can be used as a value in the interface variable.

 ⚒️ Define another method *ChangeWeight(weight uint)* in the Animal interface, does the code still work?

 ⚒️ Change the code so that the Pig struct implements the Animal interface

 ⚒️ What if the interface value is nil

 ⚒️ Separate the Animal interface and the Pig struct in different packages

## The empty interface

An interface which specifies no methods is called an *empty interface*. An empty interface may hold any value.

```go
var i interface{}
i = 0
fmt.Println(i)

i = []int{1, 2, 3}
fmt.Println(i)

i = nil
fmt.Println(i)

i = Pig{100, 2}
fmt.Println(i)
```

fmt.Print for example takes variable number of type interface{ }

## Type assertion

We use type assertion to get access to the underlying concrete value of an interface.

```go
var i interface{} = Pig{2, 100}

pig := animal.(Pig)     
num := animal.(string) // panic

var ok bool
pig, ok = animal.(Pig)     // ok is true
num, ok = animal.(string)  // ok is false
```
### Type switch
A type switch allows several type assertions in series. The switch case automatically asserts the underlying value type.

```go
func checkType(i interface{}) {
	switch v := i.(type) {
	case int:
		fmt.Printf("This is an int %d\n", v)
	case string:
		fmt.Printf("This is a string %s\n", v)
	case Pig:
		fmt.Printf("This is a pig %v\n", v)
	case Animal:
		fmt.Printf("This is an animal %v\n", v)
	default:
		fmt.Printf("I don't know about type %T!\n", v)
	}
}

func main() {
	checkType(21)
	checkType("hello")
	checkType(Pig{20, 100})
	checkType(Animal(Pig{20, 100}))
}
```

⚒️ Create a method for Pig with the signature *String() string* and then print the value and see what happens

```go
fmt.Printf("%v\n", pig)
```

Many packages look if provided types implement certain interfaces. The fmt package for example looks if the data type implements the Stringer interface and uses this to format strings.

```go
package fmt

type Stringer interface {
	String() string
}
```

⚒️ Create another struct that implements Animal but not Stringer, run it through the *checkType* function.

## Standard Interfaces

### Errors

We already talked about error handling in the Function section. Knowing the error interface is useful for creating custom errors.

```go
// In standard library
type error interface {
    Error() string
}

type MyError struct {
	When time.Time
	What string
}

func (e *MyError) Error() string {
	return fmt.Sprintf("at %v, %s",
		e.When, e.What)
}
```

### Readers

The io package specifies the io.Reader interface, which represents the read end of a stream of data. This is very popular in data manipulation.

The Go standard library contains many implementations of this interface, including files, network connections, compressors, ciphers, and others.

The io.Reader interface has a Read method: 

```go
func (T) Read(b []byte) (n int, err error)
```

Let's try using a reader, *string.NewReader(string)* returns a io.Reader interface value that can be used to read from the provided string.

```go
func main() {
	r := strings.NewReader("Hello, Reader!")

	b := make([]byte, 8)
	for {
		n, err := r.Read(b)
		fmt.Printf("n = %v err = %v b = %v\n", n, err, b)
		fmt.Printf("b[:n] = %q\n", b[:n])
		if err == io.EOF {
			break
		}
	}
}
```

⚒️ Let's create a reader method on Pig which returns and infinite stream of "Oink!"

<details><summary>Solution</summary>

```go
func (pig *Pig) Read(bytes []byte) (int, error) {
	for i := range bytes {
		bytes[i] = Oink[pig.lastOinkIndex]
		pig.lastOinkIndex = (pig.lastOinkIndex + 1) % len(Oink)
	}

	return len(bytes), nil
}
```

</details><br>

⚒️ What if we only return a number of Oinks! equal to the Pig's weight

###  io.Reader wrapper

A common pattern is an io.Reader that wraps another io.Reader, modifying the stream in some way. 

For example, the gzip.NewReader function takes an io.Reader (a stream of compressed data) and returns a *gzip.Reader that also implements io.Reader (a stream of the decompressed data). 

## File IO

Some functions from *bufio*, *os* and *io* packages to read and write data from files.

```go
// Reads entire file content into a byte slice

bytes, err := ioutil.ReadFile("a.txt")  
fmt.Println(string(bytes))
```

```go
// Reads file line by line

f, err := os.Open("a.txt")

if err != nil {
    log.Fatal(err)
}
defer f.Close()

scanner := bufio.NewScanner(f)
for scanner.Scan() {

    fmt.Println(scanner.Text())
}
```

```go
// Read file in chunks

f, err := os.Open("a.txt")

if err != nil {
        log.Fatal(err)
}
defer f.Close()

reader := bufio.NewReader(f)
buf := make([]byte, 16)

for {
    n, err := reader.Read(buf)

    if err != nil {
        if err != io.EOF {
            log.Fatal(err)
        }

        break
    } 

    fmt.Print(string(buf[0:n]))
}

```

```go
// Write entire byte slice to file

val := "Hello\n"
data := []byte(val)

err := ioutil.WriteFile("data.txt", data, 0)
if err != nil {
    log.Fatal(err)
}
```

```go
// Write string to open file

f, err := os.Create("data.txt")

if err != nil {
    log.Fatal(err)
}

defer f.Close()

_, err = f.WriteString("Hello\n")

if err != nil {
    log.Fatal(err)
}
```

```go 
// Write formatted string using the fmt printer
// This works because the open file implements io.Writer

f, err := os.Create("data.txt")

if err != nil {
    log.Fatal(err)
}

defer f.Close()

const name, age = "Johne Doe", 34

n, err := fmt.Fprintln(f, name, "is", age, "years old.")

if err != nil {

    log.Fatal(err)
}

fmt.Println(n, "bytes written")
```

⚒️ Read data from a file and write it to another

⚒️ Read numbers from a file, add a value to each of them and write them to another file

⚒️ Use a Pig struct value as a reader and an open file as a writer to add data to that file. There is a simple solution using *buffio.NewReader()*

# Next
[JSON ➡️](json.md)

