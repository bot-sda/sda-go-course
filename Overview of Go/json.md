# JSON

The Go standard library contains functions to work with JSON.

We can get struct values from bytes.
```go
// We annotate struct fields with their json name
type User struct {
	Name    string `json:"name"`
	Surname string `json:"surname"`
}

func main() {
	jsonRaw := `{"name": "John","surname": "Doe"}`
	var user User
	err := json.Unmarshal([]byte(jsonRaw), &user)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(user)
}
```

And get bytes from struct values.
```go
func main(){
    user := User{}
    user.Name = "Bob"
    user.Surname = "Lamont"
	bytes, err := json.Marshal(user)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(bytes))
}
```

```go
type User struct {
	Name    string `json:"name,omitempty"` // We can omit fields if empty
	Surname string `json:"-"`              // We can ignore fields
}
```

We can marshall slices and maps without having to wrap them.

If working with streams of data *json.Decoder* and *json.Encoder* allows us to work with data directly from a io.Reader and io.Writer
```go
var user User
decoder := json.NewDecoder(r)
decoder.Decode(&user)

encoder := json.NewEncoder(w)
encoder.Encode(user)
```

⚒️ Read JSON data from a file on disk into a structure value

## [Home 🏠](../README.md)
