# Flow

- [Flow](#flow)
	- [For](#for)
	- [If](#if)
	- [Switch](#switch)
	- [Defer](#defer)
- [Next](#next)
## For

The *for* is the only looping mechanism in Go. Like in other C-like programming languages the for loop is split in three components separated by semicolons:
- the init statement: executed before the first iteration
- the condition expression: evaluated before every iteration
- the post statement: executed at the end of every iteration

```go
func main() {
	sum := 0
	for i := 0; i < 10; i++ {
		sum += i
	}
	fmt.Println(sum)
}
```
In Go the **{ }** braces are always required after a flow component.

Let's see a **for** without initialization

```go
func main() {
	sum := 1
	for ; sum < 1000; {
		sum += sum
	}
	fmt.Println(sum)
}

//This can also be written as

func main() {
	sum := 1
	for sum < 1000 {
		sum += sum
	}
	fmt.Println(sum)
}
```

This now looks like a while in other languages.

⚒️ Create a for loop that goes from 10 to 100 and prints the numbers

⚒️ Create and print a string that says:
```
"These are my favorite numbers 3, 6, 9, 12, ...., 300"
```

## If

Like you would expect
```go
if sum == 0 {
    return 0
}

if sum < 0 {
    return -1
} else {
    return sum
}
```

Like **for**, the **if** statement can have an initialization before checking the condition.

Variables declared by the statement are only in scope in the until the end of **if**.

```go
if ok := isOk(); ok{
    ...
}
```

## Switch
Shorter way to write a sequence of **if - else**. Runs the first case whose value is equal to the condition expression and stops there, no need for brakes at the end of switch cases.

```go
func main() {
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.\n", os)
	}
}
```

The switch cases need not be constant, they get evaluated from top to bottom every time.

```go
func main() {
	fmt.Println("When's Saturday?")
	today := time.Now().Weekday()
	switch time.Saturday {
	case today + 0:
		fmt.Println("Today.")
	case today + 1:
		fmt.Println("Tomorrow.")
	case today + 2:
		fmt.Println("In two days.")
	default:
		fmt.Println("Too far away.")
	}
}
```

Because of this behavior you can write switches without a condition

```go
func main() {
	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Good evening.")
	}
}
```

## Defer

Defer keyword allows you to execute a function after the surrounding function returns. 

```go
func main() {
	defer fmt.Println("world")

	fmt.Println("hello")
}
```

This is useful when opening file handlers or streams and deferring them to close at the end of the current function.
```go
func main() {
	f, err := os.Open("data.txt")
    defer f.Close()
}
```

Defer function calls are stacked and when a function returns the calls are executed last-in-first-out.
```go
func main() {
	defer fmt.Println("hello")
	defer fmt.Println("world")
}

// Prints "world hello"
```

⚒️ Write a program that counts down from 100 to 0 using defer and a for that only increments.

# Next
[Functions ➡️](functions.md)

