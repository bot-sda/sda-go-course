# Functions

- [Functions](#functions)
	- [Arguments](#arguments)
	- [Results](#results)
	- [Returning errors](#returning-errors)
	- [First class functions](#first-class-functions)
			- [Define new type](#define-new-type)
			- [Define local function](#define-local-function)
	- [Init func](#init-func)
- [Next](#next)
  

Functions in go are defined using the keyword ***func*** and they *usually* follow this structure:

```go
func <function_name>(<parameter> type) return_type {
    ...
}
```

## Arguments

Functions can take zero or more arguments

```go
func printSmiley() {
    fmt.Println("😀")
}

func add(x int, y int) int {
	return x + y
}
```

If parameters share a type we can shorten the definition 

```go
func add(x, y int) int {
	return x + y
}
```

## Results

A function can also return zero or more results

```go
func addSubstract(x, y int) (int, int) {
	return x + y, x - y
}
```

A function can have named return values, this works similar to how the ***out*** keyword would work in other programming languages. The named return values are initialized at the start of the function and automatically returned at the end.

```go
func addSubstract(x, y int) (sum, diff int) {
	sum = x + y
    diff = x - y
    return
}
```

## Returning errors

Go does not support exception throwing 🤔. So the usual flow involves returning and ***error*** type along side our result when the action is possible to fail. 

```go
func divide(x, y int) (int, error){
    if y == 0 {
        return 0, errors.New("cannot divide by 0")
    }

    return x / y, nil
}
```

This way the caller can check if any error was returned and decide the appropriate course of action, most of the time this means returning the error and letting the root caller handle it. 

In a webserver for example this would mean returning status code 500 in the endpoint handler after a function call returns an error.

```go
func Handler(c *Context) error {
	req := new(model.GetTokenRequest)
	err := ParseJSONRequest(c, req)

    if err != nil {
        log(err)
        return c.SendStatus(500)
	}
    ...
}
```

⚒️ Call the ***divide*** function with illegal arguments and force stop the application if the function returns an error.
```go
// You can force stop the app using log.Fatal
log.Fatal("Something went terribly wrong")
```


## First class functions

In go functions can be passed as parameters defined locally and even returned by other functions.
So we can have an example where we define a function type and pass it to another function to call.

#### Define new type

```go
type add func(a int, b int) int
```

#### Define local function

```go
var a add = func(a int, b int) int {
    return a + b
}
```

⚒️ Try passing the variable ***a*** to another function ***callAdd*** and make it call ***a*** and return the result.


<details><summary>Solution</summary>

```go
package main

import "fmt"

type add func(a int, b int) int

func callAdd(myFunc add, a int, b int) int {
	return myFunc(a, b)
}

func main() {
	var a add = func(a int, b int) int {
		return a + b
	}
	fmt.Println("Sum", callAdd(a, 2, 3))
}
```
</details><br>

⚒️ What if we directly pass the function to ***callAdd*** without putting it in a variable

<details><summary>Solution</summary>

```go
package main

import "fmt"

type add func(a int, b int) int

func callAdd(myFunc add, a int, b int) int {
	return myFunc(a, b)
}

func main() {
	fmt.Println("Sum", callAdd(func(a int, b int) int {
		return a + b
	}, 2, 3))
}
```
</details><br>

⚒️ What if we create a new function that receives a parameter ***myFunc add*** and returns a new function that will get the result from ***myFunc*** and print it. We can call it ***printer***

<details><summary>Solution</summary>

```go
package main

import "fmt"

type add func(a int, b int) int

func printer(myFunc add) func(a int, b int) {
	return func(a, b int) {
		fmt.Println("Sum", myFunc(a, b))
	}
}

func main() {
	var a add = func(a int, b int) int {
		return a + b
	}

	printer(a)(2, 3)
}

```
</details><br>

## Init func 

The reserved function called `init()` will be called once when the package in which is defined gets imported, the function runs only once no matter how many times it is imported. This is useful for setting up DB connections etc.

```go
func init() {
	uploader = s3manager.NewUploader(aws_client.GetSession())
	client = s3.New(aws_client.GetSession())
}
```


# Next
[Pointers ➡️](pointers.md)

