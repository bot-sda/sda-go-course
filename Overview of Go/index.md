# Overview of Go 🧠

## [Variables](variables.md)
## [Flow](flow.md)
## [Functions](functions.md)
## [Pointers](pointers.md)
## [Structs](structs.md)
## [Slices](slices.md)
## [Maps](maps.md)
## [Packages](packages.md)
## [Methods and Interfaces](methods_and_interfaces.md)
## [JSON](json.md)
