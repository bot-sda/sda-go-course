# Packages

A Go program is made of packages. Go code which is meant to run as an executable has the entry point in the package *main*.

In order to use a package we import it using the **import** keyword

```go
import "fmt"
import "math"
```

Using functions and data types from an import is made by prefixing it with the package name.

```go
import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(math.Pi)
}
```

Because of this it is recommended to keep names short and simpl, when they are accessed they will always be prefixed by the package name.

```go
package token

func ReadToken(){    // token.ReadToken() Unnecessary

}

func Read(){        // token.Read() Clean and idiomatic 👍🏻

}
```
## Exporting 
In Go a name is exported if it begins with a capital letter. For example any functions declared in "math" that start with a lowercase letter can not be accessed from the exterior. The same goes for names defined by us.

```go
package example

func add(a int)     // not exported

func Add(a int)     // exported

type User struct {  //exported struct
	Name   string   // exported field can be accessed from outside packages
	active bool     // not exported field is only accessible from inside this package
}
```

## Experiment

⚒️ Create a new directory for our package
```bash
mkdir helper
```
⚒️ Add a new go file helper.go

⚒️ Define the new file as being in the package helper

```go
package helper
```

⚒️ Create a function that is not exported which checks if a number is even or odd.
⚒️ Create an exported function which accepts and []int and returns a new slice []int with only even numbers.

```go
in: [4, 5, 18, 12]
out: [4, 18, 22]
```

⚒️ Import the helper package in main an run it

# Next
[Methods and Interfaces ➡️](methods_and_interfaces.md)

