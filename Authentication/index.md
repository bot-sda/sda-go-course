# Authentication

- [Authentication](#authentication)
  - [Authorization](#authorization)
  - [Account creation](#account-creation)
    - [Hashing](#hashing)
    - [Rainbow tables](#rainbow-tables)
    - [Salted hashing](#salted-hashing)
  - [Example](#example)
  - [Login](#login)
  - [JWT](#jwt)
    - [How it works](#how-it-works)
    - [Generation](#generation)
    - [Parse token](#parse-token)
  - [Restricting routes](#restricting-routes)
- [Assignment](#assignment)

In computer science authentication is the process of verifying the identity of a system or user. Confirming they are who they claim they are.

Common authentication methods:
- Something you know (e.g. password, PIN)
- Something you have (e.g. phone, token device, private key)
- Something you are (e.g. fingerprint, face)

While the most common method is a password, a security conscious world is moving towards multi factor authentication.

## Authorization

While sometimes interchanged with authorization the two are different things.

Authorization is determining the permissions as user has, after confirming it's identity.

![auth](auth.png)<br>
Source: https://medium.com/geekculture/authentication-and-authorization-a5a2eafdde16

## Account creation

We are going to implement an account creation flow that will enable authentication using a username and password.

The handling method for account creation should verify the username is unique in the system and then begin the process of one way cryptographically securing the password. 

### Hashing

Hashing is the cryptographic process of computing a number, usually displayed in hexadecimal, that can not be used to calculate the input data back. 

It is used in a multitude of cryptographic systems including password storing and verification.

The reason being that we can verify if a user submitted the valid password, by using the hashing method again on the password and comparing the result with our stored data. All this without risking the passwords being discovered in case of a leak.

The properties of a hash function:
- Deterministic
- Avalanche Effect - a single byte change should create a totally different result
- One-Way Function
- Collision Resistance
- <details><summary>Quick?</summary>
  While desirable for some Hashing functions. For a password hashing algorithms slower speeds are actually favoured to help fight against brute force attacks. Checkout PKDF(Password Key Derivation Functions)
  
</details>

Hashing functions change with the advancement of computing power. Modern hashing functions like sha-3 are extremely unlikely to have  collisions(intentional or not 😅) .

### Rainbow tables

Because of the deterministic property of a hash function, multiple actors using the same function on a password will lead to the same result hash result regardless of the user or application. This would lead to entire databases filled with users having the same hashed password, compromising one password leading to all of them being compromised.

Rainbow tables were created to store the calculation results of many passwords and their calculated hash. These were and are used by hackers to crack password on leaked poorly hashed password databases.

### Salted hashing

A new technique was created called salted hashing. This involves appending a randomly generated, but not secret, string at the end of the password before hashing it.

This way even if passwords are the same across users and apps the salted hash would always differ. The salt can be stored in plain text along side the hash to be used in the verification process.

As long as the salts are truly random we have beaten rainbow tables.

```
// Pseudocode
salt = crypto.Random()
hash = crypto.Sha3(password+salt)
DB.Store({username, hash, salt})
```

## Example

In go we have a package `bcrypt` which already implements the functions we need for storing the password hash. `bcrypt.GenerateFromPassword` generates the salt and encodes it along side the hash this works fine when using the same package to verify the hash.

```go
username := "admin"
password := "hunter2"
hash, err := bcrypt.GenerateFromPassword([]byte(), bcrypt.DefaultCost)
if err != nil {
    return nil, err
}

user := &model.User{
    Username:   username,
    Hash: string(hash),
}
err = Repo.CreateUser(&user)
if err != nil {
    return nil, err
}
```

## Login

For login we will usually hash the password received and see if it matches with the record we have in the DB. Fortunately `bcrypt` has a function to makes this very easy.

```go
username := "admin"
password := "hunter2"
user, err := Repo.GetUser(username)
if err != nil {
   return nil, err
}

err = bcrypt.CompareHashAndPassword([]byte(user.Hash), []byte(password))
if err != nil {
    return nil, err
}
```

After we authenticated the user sometimes it makes sense to generate a token that he can use from now on when communicating with the server to authenticate himself, and not have to send the password every time.

More complex authentication schemes use something called a Refresh token, but we will keep it simple with a single token with a set expiry date.

## JWT

### How it works


JSON Web Token is standard which allows us to create very portable strings that contain encoded claims, in key value form, that can then be signed.

![JWT](jwt.png)<br>
Source: https://medium.com/@sureshdsk/how-json-web-token-jwt-authentication-works-585c4f076033

Signed JWTs do not secure data, they only assure who reads it that the data encoded in it was done so by the keeper of a secret key.

Modifying the claims in a Signed JWT without the secret key to sign it again will lead to a validation error when it gets read.

Schemes can vary but in our case we will keep a secret key on the server to sign new JWT tokens and then verify that they were indeed signed by us. Thus removing the need for the user to send us all the data we already verified.

JWT are very versatile, cloud providers for example use it to create limited time download links to data in buckets. https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-urls.html

### Generation

We will generate a JWT token for our user when he logs in and send it back for him, his client software should then use it to authenticate all the calls from then on.

Since JWT tokens can be verified using the secret key we have on the server we don't need to store it in the DB.


Install package we need for JWT manipulation
```go
go get -u github.com/golang-jwt/jwt/v4
```

Example

```go
func CreateToken(username string) (string, error) {
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": username,
		"expiry":   time.Now().Add(24 * time.Hour).Unix(),
	})

	// Sign and get the complete encoded token as a string using the secret
	return token.SignedString([]byte(key))
}
```

### Parse token

```go
func ParseToken(tokenString string) (map[string]interface{}, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(key), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, err
	}
}
```

## Restricting routes

Since now we expect the user to send us an auth token for every call to a sensitive endpoint we will have to restrict routes in our server

We do this using middleware, a special function that runs before certain handlers. An auth middleware will check the validity of a JWT  and decide if a call should continue to the handler or not.

```go
func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
    t := time.Now()

    // Get token from header
    token := c.Request.Header["Token"]

    // Check token validity
    ...

    // Set username as context for this request, we can access this in our handlers
    c.Set("username", claim["username"])
    
    // Pass the request to the handlers
    c.Next()    
  }
}


func main(){
  ...
  authorized := r.Group("/private")
  authorized.Use(AuthRequired()) // Use a defined middleware
  {
    authorized.POST("/restrictedPath", endpoint1)
    authorized.POST("/restrictedPath2", endpoint2)
  }
  ...
}
```

# Assignment

Keep it simple by only implementing the changes for the pure API endpoints, not the rendered ones.

- Add a new login endpoint "/login" that accepts a POST request with a username and password and returns a signed JWT token for the username
- Change the path for all sensitive endpoints to start with "/private"
- Add an auth middleware which checks the validity of the Bearer token for every path starting with "/private"
