# Create project

First let's create a directory for our project

```bash
mkdir hello
cd hello
```

Then we will use the Go utility to initialize a new module, which will be our application.

```bash
go mod init example/hello
```

This will create a new go.mod file which contains the expected running version of Go and package dependencies

![go.mod](go.mod.png)

Let's open vscode and create a new go file, make sure the terminal is in the *hello* directory.

```bash
code .
```

All executable Go projects have the entry point in the **main** function of the **main** package.

And since our first project will be an executable we will create a new file in which we'll host the main function.

```go
package main // since this is our application's entry point we define the package as main

import "fmt"

func main() {
    fmt.Println("Hello, World!")
}

```

"fmt" is a standard package used for formatting and I/O operations.

Now let's run the project using

```go
❯ go run .
Hello world
```

### Great Success!!

We can build the program using the build command. An environment variable defines the target build architecture, so you could do this in a linux machine for Continuous Development for example

```bash
# 64-bit macOS
$ GOOS=darwin GOARCH=amd64 go build -o bin/app-amd64-darwin app.go

# 64-bit linux
$ GOOS=linux GOARCH=amd64 go build -o bin/app-amd64-linux app.go

# windows
$ GOOS=windows GOARCH=386 go build -o bin/app-386.exe app.go
```

## [Home 🏠](../README.md)