# Installation and Resources 📐

- [Installation and Resources 📐](#installation-and-resources-)
  - [Installation](#installation)
    - [Go](#go)
    - [IDE](#ide)
      - [VsCode](#vscode)
      - [GoLand](#goland)
      - [Packages](#packages)
  - [Resources](#resources)
  - [Creating our first project ➡️](#creating-our-first-project-️)

## Installation

### Go


Go is straightforward to install, just go to the official download site https://go.dev/doc/install and use the appropriate installer depending on the OS.

<details ><summary>Linux and Mac</summary>

Installing on Linux or Mac requires manual modification of the PATH environment variable to point at "/usr/local/go/bin". 

</details>  <br>


To make sure everything is installed correctly we'll run ***go version***

If no error is displayed and we get the version number everything should work correctly

```go
> go version
go version go1.19 windows/amd64
```


### IDE

For this course we will use Visual Studio Code as the primary IDE for writing Go.

#### VsCode
Vscode can be installed from https://code.visualstudio.com/. 

After installing vscode I hingly recommend the Go plugin 

![Go plugin](/Installation%20and%20Resources/plugin.png)

After opening a go file this should also prompt you to install some go utilities that will be used by the extension for linting and debugging. 


#### GoLand
An alternative IDE for those familiar with the Jetbrains product is also GoLand https://www.jetbrains.com/go/


#### Packages

Any packages we need will get installed from the command line using the Go utility.


## Resources

The official Go Documentation page https://go.dev/doc/

Tour of Go(great refresher on syntax) https://go.dev/tour/basics/

Standard library Documentation https://pkg.go.dev/std

More in depth language specification https://go.dev/doc/effective_go

## [Creating our first project ➡️](create_project.md)