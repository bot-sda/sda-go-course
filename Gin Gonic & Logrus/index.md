# Gin Gonic 🍸

While entirely possible to use the standard Go library to write a perfectly good webserver, popular frameworks offer nice support for things like middleware and routing. 

Gin gonic is a web framework for Go designed for performance and productivity. In the next section we will create a small e-commerce webserver that juggles data in memory to get a feel for the framework.

https://github.com/gin-gonic/gin

<img src="img/gin.png" alt="gin" width="200"/>

# Logging 🪵

While reasonable to use the standard library for the webserver I wouldn't recommend the standard logging library for anything more than a small project. 

### Structured logging

The standard logging library only supports dumbing raw strings as logs so most of the time it makes the most sense to use a library that is able to support structured logging.

Modern logging and tracing would't be possible without structured logging. 
 - It allows fast querying of logging data with a service like Elastic-Search
 - Allows analytics and alerts with something like Grafana
 - Can be fed into a SIEM server for security analysis and response

<img src="img/elastic-search.png" />
Source: https://learn.microsoft.com/en-us/dotnet/architecture/cloud-native/logging-with-elastic-stack
<br>

<img src="img/grafana.png" />
Source: https://scaleyourapp.com/what-is-grafana-why-use-it-everything-you-should-know-about-it/
<br>

<img src="img/siem.png" />
Source: https://www.logpoint.com/en/understand/what-is-siem/

## Logrus 

We will be using the Logrus library it is extremely popular and easy to use. 

https://github.com/sirupsen/logrus

It also has an animal mascot <br>
<img src="img/walrus.png"  width="80"/>

## [Let's start ➡️](1.md)