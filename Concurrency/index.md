# Table of contents
- [Table of contents](#table-of-contents)
- [Goroutines 🧵](#goroutines-)
		- [Go is concurent not parallel](#go-is-concurent-not-parallel)
- [Channels 🛣️](#channels-️)
	- [Buffered Channels](#buffered-channels)
	- [Range](#range)
	- [Close](#close)
	- [Select](#select)
		- [Default](#default)
	- [Mutex](#mutex)

# Goroutines 🧵

Gotoutines are the primary mechanism through which Go achieves concurrency.

### Go is concurent not parallel
> But when people hear the word concurrency they often think of parallelism, a related but quite distinct concept. In programming, concurrency is the composition of independently executing processes, while parallelism is the simultaneous execution of (possibly related) computations. Concurrency is about dealing with lots of things at once. Parallelism is about doing lots of things at once.

https://go.dev/blog/waza-talk

A goroutine is a lightweigh thread managed by the Go runtime. Starting a new goroutine is as easy as
```go
go doSomething()
```
Goroutines get killed off if the application stops running so make sure to synchronize if you want to wait for their execution.

# Channels 🛣️

Channels are the primary mechanism for synchronization in Go, they rely on data flow and achieve synchronization without locks.

```go
ch <- v    // Send v to channel ch.
v := <-ch  // Receive from ch, and
           // assign value to v
```

To create a channel we use the *make* keyword passing along the data type being transferred across the channel.

```go
ch := make(chan int)
```

Channels by default block the flow of execution until data is passed, if sending until the data is sent and if receiving until the data is received. 

```go
// Summing 2 halfs of the same slice at the same time

func sum(s []int, c chan int) {
	sum := 0
	for _, v := range s {
		sum += v
	}
	c <- sum // send sum to c
}

func main() {
	s := []int{7, 2, 8, -9, 4, 0}

	c := make(chan int)
	go sum(s[:len(s)/2], c)
	go sum(s[len(s)/2:], c)
	x, y := <-c, <-c // receive from c

	fmt.Println(x, y, x+y)
}
```

## Buffered Channels

Channels can have an internal buffer, this allows for the write actions to not become blocking until the buffered is filled.

```go
ch := make(chan int, 100)
```

```go
// This program freezez because it is waiting for a channel reader
func main() {
	ch := make(chan int)
	ch <- 1
	ch <- 2
	fmt.Println(<-ch)
	fmt.Println(<-ch)
}
```

```go
// This program works fine because the channel can store values
func main() {
	ch := make(chan int, 2)
	ch <- 1
	ch <- 2
	fmt.Println(<-ch)
	fmt.Println(<-ch)
}
```

## Range

We can range over a channel like we would a slice, running the loop every time data is received

```go
c := make(chan int)
go calculateNumbers(c)

for i := range c {
	fmt.Println(i)
}
```

⚒️ Implement calculateNumber add some numbers to the channel

## Close

We can close a channel to indicate no more data will be sent. Any *for* that has a range over the channel stops when that channel is closed. 

Testing if the channel is closed

```go
v, ok := <-ch
```

⚠️ Only the sender should close a channel. Sending over a closed channel causes a panic.

## Select

The *select* statement defines multiple operations for a channel, letting the goroutine wait until at least one operation is available, if more than one case can run one is chosen randomly.


```go
func sendNumbers(c chan int) {
	i := 0
	for {
		c <- i
		i++
		time.Sleep(100 * time.Millisecond)
	}
}

func killApp(quit chan bool) {
	time.Sleep(3 * time.Second)
	quit <- true
}

func main() {
	c := make(chan int)
	quit := make(chan bool)
	go sendNumbers(c)
	go killApp(quit)

	for {
		select {
		case x := <-c:
			fmt.Println("Number received ", x)
		case <-quit:
			return
		}
	}
}
```

### Default 

The default case runs if no other case is ready.

```go
select {
case x := <-c:
    fmt.Println("Number received ", x)
case <-quit:
    return
default:
    fmt.Println("Waiting...")
    time.Sleep(50 * time.Millisecond)
}
```

⚒️ Create two goroutines, one that sends even numbers every 100 milliseconds on a chan and one that sends odd numbers every 200 milliseconds on another chan. Print them in main

⚒️ Create a deadlock using goroutines

⚒️ Create a snippet which times the execution time of a function and sends it to a goroutine to be logged

⚒️ (Extra) Implement merge sort using goroutines

⚒️ (Super Extra) Implement a concurrent web crawler


## Mutex

Channels are focused on synchronization using communication, but if we need mutual exclusion on a shared resource we can use a mutex.

```go
var mu sync.Mutex
mu.Lock()
mu.Unlock()
```
