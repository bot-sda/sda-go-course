# Code Generation 🧬

The `go generate` command works by scanning for special comments in the Go source code, that specify how code should be generated. 

It’s important to understand that go generate is not part of go build. It contains no dependency analysis and must be run explicitly before running go build. It is intended to be used by the author of the Go package, not its clients.

The command `go generate` is looking for comments like this
```go
//go:generate goyacc -o gopher.go -p parser gopher.y
```

In the example above `go generate` will run the `goyacc` tool to generate some go parser code.

Generate can obviously only run an executable if it's accessible either locally or through the OS PATH.

## Example

Let's use a utility called `stringer` which generates go code to be able to pretty print constant values of a type for logging and whatnot.

First let's create a new `int` type for Seasons and also create constant values for each particular season. This can be thought as similar to enums in go.

```go
type Season int

const (
	Summer Season = iota   // 0 iota initializes the first constant value with 0 and then each new one with 1 more
	Autumn                 // 1
	Winter                 // 2
	Spring                 // 3
)
```

If we were to print any const value using `fmt` we would get the numerical value, something we probably don't want. We could of course create a manual Stringer function that can print these values but it would need to be modified each time we add or change a value.

```go
fmt.Println(Summer)  // Prints 0 :(
```

So to make things automatic we will add a `go generate` command and also install the `stringer` utility.

```go
go install golang.org/x/tools/cmd/stringer // go install compiles the stringer code 
                                            // and adds the executable to the go bin path
```

And let's add this directive to the file where we defined the Seasons

```go
//go:generate stringer -type=Season

type Season int

const (
	Summer Season = iota   
	Autumn                 
	Winter                 
	Spring                
)
```

Now if we run `go generate` in the terminal we'll see that `stringer` generated a new go code file called `season_string.go`. 
The code looks awful but it's ok, you should't look at or touch it anyway.

🏃🏻‍♂️ Try printing the `Summer` const value now and see what happens!

## Exercises

⚒️ Write a `go generate` directive that generates a text file containing the local files(or the local IP) ⚒️

⚒️ Write a go program that generates a go file which prints "Hello world", compile it and run it with a `go generate `directive ⚒️

## [YACC 🦬 ➡️](yacc.md)
