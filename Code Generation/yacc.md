# YACC 🦬

Yacc (Yet Another Compiler-Compiler) is a program originally created by Stephen C. Johnson. It is a Look Ahead Left-to-Right Rightmost Derivation (LALR) parser generator based on formal grammar. The formal grammar contains, in our case, snippets of Go code attached to rules.

<img src="yacc.jpg" width="400" /><br>
Source: https://www.tutorialandexample.com/yacc

The YACC tool will use this grammar to generate code which can parse a specific input. 

## You may use this to
- Write a compiler
- Write a parser for a specific data structure
- Write a state machine

Install `goyacc`
```go
go install golang.org/x/tools/cmd/goyacc
```

Checkout the json [parser example](parser)

⚒️ Generate the parser code and test it ⚒️

## [Home 🏠](../README.md)