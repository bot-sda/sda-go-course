# Testing 🧪

Test functions follow a standard structure to let the CLI tool know what to run when testing. 

```go
package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetPackageDetailsRoute(t *testing.T) {
    ...
}
```

We are passing a `testing.T` pointer that we will use when asserting. For asserting we install a new package.
```go
go get -u github.com/stretchr/testify
```

This grants us access to a multitude of assert functions. The library also allows creation of mocks that can be tested for calls, we won't go into that in this course.

```go
import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test(t *testing.T) {
    assert.Equal(t, 1 , 2) // This will make the test fail
}
```

## Running tests
### IDE
Tests can be ran via the interface in the Vscode IDE.<br>
![run test](run_test.png)
<br>

### Terminal
Or in the command line using the `go test` toolchain tool directly

```go
// Runs all test found recursively in current directory 
go test ./...  
```

## Assignment

Let's continue our project by creating a test file for every handler file, so for `product.go` we make `product_test.go` and so on. Then we are going to write a test for every endpoint we created previously.

For now we can keep it at only happy paths to get and idea of how testing works.

In order to test http endpoints we'll need a recorder that tracks response data. Let's look at an example.

```go
package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
)

func TestGetPackageDetailsRoute(t *testing.T) {
	router := setupRouter()                           // Setup router with mocked handler

	w := httptest.NewRecorder()                       // Setup recorder
	req, _ := http.NewRequest(http.MethodGet, "/api/product/1", nil) // Make HTTP call
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)          // Check response code is 200
	assert.Equal(t, "{\"productId\":\"1\"}", w.Body.String()) // Check response body is the JSON we expected
}

func setupRouter() *gin.Engine {
	logger, _ := test.NewNullLogger()
	h, _ := New(logger)
	r := gin.Default()
	r.GET("/api/product/:id", h.GetProductDetails)

	return r
}
```

## Bonus

Test if endpoint is logging enough

```go
func TestSomething(t*testing.T){
  logger, hook := test.NewNullLogger()
  logger.Error("Helloerror")

  assert.Equal(t, 1, len(hook.Entries))
  assert.Equal(t, logrus.ErrorLevel, hook.LastEntry().Level)
  assert.Equal(t, "Helloerror", hook.LastEntry().Message)

  hook.Reset()
  assert.Nil(t, hook.LastEntry())
}
```

## [Rendering ➡️](rendering.md)
