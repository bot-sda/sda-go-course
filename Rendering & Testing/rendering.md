
# HTML Rendering 🎨

HTML rendering is a mode for Go to use HTML template files and server data to generate a fully formed webpage to send to the user.

![SSR](ssr.png)

This is certainly easy to spin up for a small project, but a larger and more complex project requires a lot more thought into deciding the rendering strategy. Things like the reactivity of the app, the secrecy of the code-base, the time to first paint etc. must be taken into consideration. This is a good read https://web.dev/rendering-on-the-web/.

## But for our small assignment this will work perfectly 🌍

Templates are written like plain HTML with with certain structures that when fed into the renderer populates with data.

This template
```html
<html>
	<h1>
		{{ .title }}
	</h1>
</html>
```

Passed to the renderer with the following data

```go
router.GET("/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"title": "Main website",
		})
})
```

Creates the following page

```html
<html>
	<h1>
		Main website
	</h1>
</html>
```

Some of the more popular structures are, with the full list here https://pkg.go.dev/text/template#hdr-Actions
```
{{/* a comment */}} 	Defines a comment
{{.}} 	Renders the root element
{{.Title}} 	Renders the “Title”-field in a nested element
{{if .Done}} {{else}} {{end}} 	Defines an if-Statement
{{range .Todos}} {{.}} {{end}} 	Loops over all “Todos” and renders each using {{.}}
{{block "content" .}} {{end}} 	Defines a block with the name “content”
```

## Assignment

It's time to implement the webpages to our e-commerce site. We'll access the same data that we did for our API in the previous tasks but this time we'll fill some HTML templates with it.


## [Next ➡️](1.md)


