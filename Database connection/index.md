# Database connection 💽

## Install PostgreSQL

Navigate to https://www.postgresql.org/download/ and download the installer specific to your OS.

A refresher on SQL https://www.sqltutorial.org/sql-cheat-sheet/.

## GORM

Is a ORM library fo Go, it has connectors for a multitude of databases and is pretty easy to use. It will helps us create database tables diretly from code models and query directly into structs.

### Install
```go
go get -u gorm.io/gorm
go get -u gorm.io/driver/postgres
go get github.com/joho/godotenv
```

### Connect

#### Postgres
```go
import (
  "gorm.io/driver/postgres"
  "gorm.io/gorm"
)

dsn := "host=localhost user=gorm password=gorm dbname=gorm port=9920"
db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
```

#### MySql
```go
import (
  "gorm.io/driver/mysql"
  "gorm.io/gorm"
)

func main() {
  dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
  db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
}
```
### Create an .env file

To make changing the connection parameters more managable let's add the fields we are going to use to a `.env` file in our project root.

```go
_ = godotenv.Load()
username := os.Getenv("dbUsername")
password := os.Getenv("dbPassword")
dbName := os.Getenv("dbName")
dbHost := os.Getenv("dbHost")
dbPort := os.Getenv("dbPort")

dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s", dbHost, username, password, dbName, dbPort)
db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
```

[Integrating ➡](1.md)


