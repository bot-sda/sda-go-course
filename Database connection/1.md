# Integrating DB

## Create package for database connection logic

Let's create a new package called `db` which will have a function for connecting to the database and returning a `*gorm.Db` interface value.

## Add db connector to Handler

The server handlers will need access to the database, so lets add that dependency in the Handler struct.

⚠ Is this correct? Let's see what happens.

- Will our tests compile?
- Will we be able to mock the db connector?
- What would make more sense?

## Adding another abstraction layer

It would be unnecessary complicated to mock our DB connection for our tests, in this case it would make the most sense to add another abstraction layer in the form of a `repo`.

The `repo` will also host our database querying logic keeping the code tidy.

### Create a Repo interface

Create an empty Repo interface for now and add that instead of the DB connection in the `Handler` struct. 

Add a concrete `repo` struct that will contain the querying logic methods. We can also not export this struct as this package will be the only one creating it.

<details>

```go
type Repo interface {
	GetProducts(offset, limit int) ([]model.Product, error)
}

type repo struct {
	DB *gorm.DB
}

func New() (Repo, error) {
	db, err := NewDB()
	if err != nil {
		return nil, err
	}

	return &repo{
		DB: db,
	}, nil
}

func (r *repo) GetProducts(offset, limit int) ([]model.Product, error) {
	...
}
``` 
</details><br>

Now we can create a mock struct that implements `Repo` in our test cases and return whatever data we want. Great success!


## Declaring models

### Conventions
GORM prefers convention over configuration. By default, GORM uses ID as primary key, pluralizes struct name to snake_cases as table name, snake_case as column name, and uses CreatedAt, UpdatedAt to track creating/updating time.

### gorm.Model

GORM defined a gorm.Model struct which we can embed in our models.

```go
// gorm.Model definition
type Model struct {
  ID        uint           `gorm:"primaryKey"`
  CreatedAt time.Time
  UpdatedAt time.Time
  DeletedAt gorm.DeletedAt `gorm:"index"`
}

type Package struct{
    gorm.Model // Adds the gorm.Model fields to our struct
    Name string
    Price int
}

...
// AutoMigrate makes sure the database tables are up to date with the model
func init(){
   db.AutoMigrate(&Package{})
}
```

When following the GORM conventions minimal code has to be written. If adapting a model after a DB already created modifiers to the table GORM expects can be made using tags.

The tables *should* get created when running the code first. Migration is possible but for small projects just drop the DB and let GORM create it again.

More details on tags https://gorm.io/docs/models.html#Fields-Tags

### Foreign keys One-Many

GORM automatically tries to identify relations between model structs by looking at Id fields, if a struct contains the id of another struct name it assumes this is a foreign key.

```go
type User struct {
  gorm.Model
  CreditCards []CreditCard
}

type CreditCard struct {
  gorm.Model
  Number string
  UserID uint
}
```

The library also support Back-Reference and foreign key overriding, check docs for more details. https://gorm.io/docs/many_to_many.html

## CRUD

### Create

Single
```go
user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}

result := db.Create(&user) // pass pointer of data to Create

user.ID             // returns inserted data's primary key
result.Error        // returns error
result.RowsAffected // returns inserted records count
```
Multiple
```go
var users = []User{{Name: "jinzhu1"}, {Name: "jinzhu2"}, {Name: "jinzhu3"}}
db.Create(&users)
```

### Query

Select
```go
// Get the first record ordered by primary key
db.First(&user)
// SELECT * FROM users ORDER BY id LIMIT 1;

db.First(&user, "10")
// SELECT * FROM users WHERE id = 10;

db.Find(&users, []int{1,2,3})
// SELECT * FROM users WHERE id IN (1,2,3);

// Get one record, no specified order
db.Take(&user)
// SELECT * FROM users LIMIT 1;

// Get last record, ordered by primary key desc
db.Last(&user)
// SELECT * FROM users ORDER BY id DESC LIMIT 1;

result := db.First(&user)
result.RowsAffected // returns count of records found
result.Error        // returns error or nil

// check error ErrRecordNotFound
errors.Is(result.Error, gorm.ErrRecordNotFound)
```

Where
```go
// Get first matched record
db.Where("name = ?", "jinzhu").First(&user)
// SELECT * FROM users WHERE name = 'jinzhu' ORDER BY id LIMIT 1;

// Get all matched records
db.Where("name <> ?", "jinzhu").Find(&users)
// SELECT * FROM users WHERE name <> 'jinzhu';

// IN
db.Where("name IN ?", []string{"jinzhu", "jinzhu 2"}).Find(&users)
// SELECT * FROM users WHERE name IN ('jinzhu','jinzhu 2');

// LIKE
db.Where("name LIKE ?", "%jin%").Find(&users)
// SELECT * FROM users WHERE name LIKE '%jin%';

// AND
db.Where("name = ? AND age >= ?", "jinzhu", "22").Find(&users)
// SELECT * FROM users WHERE name = 'jinzhu' AND age >= 22;

// Time
db.Where("updated_at > ?", lastWeek).Find(&users)
// SELECT * FROM users WHERE updated_at > '2000-01-01 00:00:00';
```

Not and Or

```go
db.Not("name = ?", "jinzhu").First(&user)
// SELECT * FROM users WHERE NOT name = "jinzhu" ORDER BY id LIMIT 1;

db.Where("role = ?", "admin").Or("role = ?", "super_admin").Find(&users)
// SELECT * FROM users WHERE role = 'admin' OR role = 'super_admin';
```

Order 
```go
db.Order("age desc, name").Find(&users)
// SELECT * FROM users ORDER BY age desc, name;

// Multiple orders
db.Order("age desc").Order("name").Find(&users)
// SELECT * FROM users ORDER BY age desc, name;
```

Joins Preloading
```go
db.Joins("Company").Find(&users)
// SELECT `users`.`id`,`users`.`name`,`users`.`age`,`Company`.`id` AS `Company__id`,`Company`.`name` AS `Company__name` FROM `users` LEFT JOIN `companies` AS `Company` ON `users`.`company_id` = `Company`.`id`;


db.Joins("Company", db.Where(&Company{Alive: true})).Find(&users)
// SELECT `users`.`id`,`users`.`name`,`users`.`age`,`Company`.`id` AS `Company__id`,`Company`.`name` AS `Company__name` FROM `users` LEFT JOIN `companies` AS `Company` ON `users`.`company_id` = `Company`.`id` AND `Company`.`alive` = true;

// Or explicit
db.Model(&User{}).Select("users.name, emails.email").Joins("left join emails on emails.user_id = users.id").Scan(&result{})
```


### Update
```go
db.First(&user)

user.Name = "jinzhu 2"
user.Age = 100
db.Save(&user)
```

### Delete
```go
// Email's ID is `10`
db.Delete(&email)
// DELETE from emails where id = 10;

// Delete with additional conditions
db.Where("name = ?", "jinzhu").Delete(&email)
// DELETE from emails where id = 10 AND name = "jinzhu";

db.Delete(&User{}, 10)
// DELETE FROM users WHERE id = 10;

db.Delete(&User{}, "10")
// DELETE FROM users WHERE id = 10;

db.Delete(&users, []int{1,2,3})
// DELETE FROM users WHERE id IN (1,2,3);
```

### Soft Delete

If your model includes a gorm.DeletedAt field, it will get soft delete ability automatically.

When calling Delete, the record WON’T be removed from the database, but GORM will set the DeletedAt‘s value to the current time, and the data is not findable with normal Query methods anymore.

Examples from and more details at https://gorm.io/docs/query.html

[Assignment ➡](2.md)

