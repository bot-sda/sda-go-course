/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "example/cobra/cmd"

func main() {
	cmd.Execute()
}
