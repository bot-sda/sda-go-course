/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var Name string

// helloCmd represents the hello command
var helloCmd = &cobra.Command{
	Use:   "hello",
	Short: "This command greets the user",
	Long: `This command uses machine learning and advanced graph network algorthms 
	to greet the user with a personal message. 
	User must provide name tho.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello ", Name)
	},
}

func init() {
	rootCmd.AddCommand(helloCmd)

	helloCmd.Flags().StringVarP(&Name, "name", "n", "", "Name to greet")
	helloCmd.MarkFlagRequired("name")
}
