# Introduction 👋🏻


## What is Go

Go, or Golang for when the Google search doesn't return good results, is an open-source programming language designed by Google.

As we are going to see Go is a *cute* and easy to learn language, containing much less **magic** than other languages being at times bluntly explicit.


  ![Gopher](/Introduction/gopher.png)

## Where is Go used

- Webapp and mobile backend servers
- CLI and stand-alone tools
- Distributed network services
- Cloud native computing
- CI/CD tools

## Why
 
- Speed - almost as fast as C but with Garbage collection
- Fast Compilation to machine code - perfect for controlled environments
- Statically types
- Easy to learn
- Built for network and concurrency

## [Home 🏠](../README.md)