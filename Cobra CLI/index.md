# Cobra CLI 🐍

Cobra is a Go library for creating CLI applications.

In order to use cobra we'll need to import the library.

```
go get -u github.com/spf13/cobra@latest
```

The library comes with a cli of it's own, cobra-cli. We'll use it to generate code for our application, making it easier to get started and add new commands.

```
go install github.com/spf13/cobra-cli@latest
```

## Creating a project

First we create an empty go module 

```
mkdir cobra
cd cobra
go mod init example/cobra
```

If we have the cobra-cli tool we can initialize the project using this command

```
cobra-cli init
```

This will create a project skeleton we can get started on.

## Adding commands

A command will have a parent, root if none is provided, and any number of children.

To add a new command using the cobra-cli tool we will run

```
// Adds a new command "start"
cobra-cli add start
```

If cobra-cli isn't working you can look at how commands are defined in the provided project. Or check the official documentation.

If we want to provide a parent for our command we use the `-p` flag

```
// Adds a new subcommand "start service"
cobra-cli add service -p 'startCmd'
```
By default cobra-cli will append Cmd to the name provided and uses this name for the internal variable name. When specifying a parent, be sure to match the variable name used in the code.

⚠ Be sure to only use camelCase for command names, cobra doesn't support snake_case or kebab-case.


## Run commands

We can now test our commands and see if they work.

```
> go run . start
start called

> go run . start service
service called

// We can also build the app and run it this way

> go build .
> ./cobra.exe start service
service called
```

## Command definition

Let's look at the command definition cobra-cli created.

```go
var serviceCmd = &cobra.Command{
	Use:   "service",
	Short: "Will start a named service",
	Long: `A longer description that spans multiple lines and likely contains examples.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("service started ", Name)
	},
}
```

We can see that the command is created with information that will be returned to the user when he uses help. Also a function that is called when the command is run.

There are a number of Run functions accepted by the command definition.

The `Persistent*Run` functions will be inherited by children if they do not declare their own. These functions are run in the following order:
```
PersistentPreRun 
PreRun   
Run
PostRun
PersistentPostRun
```

If the function has the possibility of returning an error we can use `RunE` instead of `Run`, `PreRunE` instead of `PreRun` and so on.

## Flags

Flags are accessed at run time by the command using a variable. This means we have to define are flags separately, and if multiple commands use them make sure they can be accessed.

```go
var Verbose bool
var Name string
...

// Local flag only to this command
serviceCmd.Flags().StringVarP(&Name, "name", "n", "", "Name of the service to start")

// Persistent flags are available in this command and all subcommand
rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")
```

Let's mark the flag as required

```go
// Local
serviceCmd.MarkFlagRequired("name")

// Persistent
rootCmd.MarkPersistentFlagRequired("...")
```

## Flag groups

If we have different flags that must be provided together we can enforce that with flag groups. Like a username and password

```go
rootCmd.Flags().StringVarP(&u, "username", "u", "", "Username (required if password is set)")
rootCmd.Flags().StringVarP(&pw, "password", "p", "", "Password (required if username is set)")
rootCmd.MarkFlagsRequiredTogether("username", "password")
```

We can also prevent flags from being provided together if they represent exclusive options. Like the output type

```go
rootCmd.Flags().BoolVar(&u, "json", false, "Output in JSON")
rootCmd.Flags().BoolVar(&pw, "yaml", false, "Output in YAML")
rootCmd.MarkFlagsMutuallyExclusive("json", "yaml")
```

## Arguments

Arguments passed will accessible in the handling function as the `args` string slice.

We can add restrictions for number of arguments.


- NoArgs - report an error if there are any positional args.
- ArbitraryArgs - accept any number of args.
- MinimumNArgs(int) - report an error if less than N positional args are provided.
- MaximumNArgs(int) - report an error if more than N positional args are provided.
- ExactArgs(int) - report an error if there are not exactly N positional args.
- RangeArgs(min, max) - report an error if the number of args is not between min and max.

But actual arguments data validation must be made in the handler.

## Exercises

⚒ Write a command `hello` with a `-n --name` flag that prints the message: "Hello <name>"
	
⚒ Write a command `create` with two subcommands `file` and `folder`. `create file` creates a file, `create folder` creates a folder
	
⚒ Write a command `web` which asks the OS to opens "http://google.com" 
	


## Resources
Cobra supports other things like shell autocomplete, help overriding, example usages, suggestions. For more information check their github https://github.com/spf13/cobra.<br>
Pretty prompting with promptui https://github.com/manifoldco/promptui
