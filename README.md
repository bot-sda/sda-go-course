# Golang course

## Modules
  - [Introduction](Introduction/index.md) 
  - [Installation & Resources](Installation%20&%20Resources/index.md) 
  - [Overview of Go](Overview%20of%20Go/index.md) 
  - [Rest & HTTP](Rest%20&%20HTTP/index.md) 
  - [Concurrency](Concurrency/index.md) 
  - [Gin Gonic & Logrus](Gin%20Gonic%20&%20Logrus/index.md) 
  - [Rendering & Testing](Rendering%20&%20Testing/index.md) 
  -  [Code Generation](Code%20Generation/index.md) 
 - [Database connection](Database%20connection/index.md)
 - [Authentication](Authentication/index.md)
 - [Working with CLI](Cobra%20CLI/index.md)

